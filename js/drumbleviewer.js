$(document)
.ready( function() {

	// functions
	// hide all empty channels by default
	jQuery.fn.oz_hide_all = function() {
		$('.empty').hide();

	};
	jQuery.fn.oz_sort_all = function() {
		$("div[id^='cb-']")
		.each(
				function() {
					cb = $(this);
					sid = $(cb).attr('id');

					anid = $('#c-' + sid);

					if (anid.hasClass('user_in')) {
						if ($(cb).is('.tree_plus_end')) {
							$(cb)
							.removeClass(
									"tree_plus")
									.addClass(
									'tree_minus_end');
						} else {
							$(cb).removeClass("tree_plus")
							.addClass('tree_minus');
						}

						$(cb)
						.parents()
						.filter('.childbox')
						.each(
								function() {
									lol = $(this)
									.attr(
											'id')
											.substr(
													2);
									if ($("#" + lol)
											.is(
											'.tree_plus_end')) {
										$("#" + lol)
										.removeClass(
										"tree_plus")
										.addClass(
										'tree_minus_end');
									} else {
										$("#" + lol)
										.addClass(
										'tree_minus');
									}
								});
					}
				});

		$('.user_in').parents().filter('.childbox').show();
	};

	// show/hide channelblocks
	jQuery.fn.oz_sh_chanblocks = function() {
		$("div[id^='cb-']").click(
				function(e) {
					var value;
					value = $(this);
					id = value.attr('id');
					inner = $('#c-' + id);
					if (inner.is(':visible')) {
						if ($(this).is('.tree_minus_end')) {
							inner.slideUp("fast");
							value.removeClass("tree_minus_end")
							.addClass('tree_plus_end');
						} else {
							inner.slideUp("fast");
							value.removeClass("tree_minus")
							.addClass('tree_plus');
						}
					} else {
						if ($(this).is('.tree_plus_end')) {
							inner.slideDown("fast");
							value.removeClass('tree_plus_end')
							.addClass('tree_minus_end');
						} else {
							inner.slideDown("fast");
							value.removeClass('tree_plus')
							.addClass('tree_minus');
						}
					}

					e.preventDefault();

				});
	};
	// username tooltips
	jQuery.fn.oz_usertooltips = function() {
		$('a.load-ustats').cluetip( {
			local :true,
			cursor :'pointer',
			tracking :true,
			width :180,
			hoverIntent : {
			sensitivity :1,
			interval :50,
			timeout :0
		},
		positionBy :'mouse',
		fx : {
			open :'fadeIn', // can be 'show' or 'slideDown' or 'fadeIn'
			openSpeed :'normal'
		}

		});
		$('a.load-mv_footbox').cluetip( {
			local :true,
			cursor :'pointer',
			tracking :true,
			width :180,
			hoverIntent : {
			sensitivity :1,
			interval :50,
			timeout :0
		},
		positionBy :'mouse',
		fx : {
			open :'fadeIn', // can be 'show' or 'slideDown' or 'fadeIn'
			openSpeed :'normal'
		}

		});
	};
	//init
	$('.mumbleviewer').oz_hide_all();
	$('.mumbleviewer').oz_sort_all();
	$('.mumbleviewer').oz_usertooltips();
	$('.mumbleviewer').oz_sh_chanblocks();

	//end
});