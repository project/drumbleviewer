<?php

/**
 * @file
 * The drumbleviewer setting.
 *
 * settings for the drumbleviewer
 */

function drumbleviewer_settings() {
  $form ['drumbleviewer_serverid'] = array (
    '#type' => 'textfield',
    '#title' => t ( 'Server ID' ),
    '#description' => t ( 'The Server ID. Default is 1' ),
    '#size' => 2,
    '#default_value' => variable_get ( 'drumbleviewer_serverid', 1 ),
    '#maxlength' => 100,
  );
  
  $form ['drumbleviewer_footermessage'] = array (
    '#type' => 'textfield',
    '#title' => t ( 'Footermessage' ),
    '#description' => t ( 'Footmessage' ),
    
    '#default_value' => variable_get ( 'drumbleviewer_footermessage', 'powered by MumbleViewer' ),
    '#maxlength' => 255,
  );

  $form ['drumbleviewer_serverurl'] = array (
    '#type' => 'textfield',
    '#title' => t ( 'Server URL or IP' ),
    '#description' => t ( 'The URL to join the Server. WITHOUT http://' ),
    
    '#default_value' => variable_get ( 'drumbleviewer_serverurl', 'example.com' ),
    '#maxlength' => 255,
  );
  
  $form['drumbleviewer_shownode'] = array(
    '#type' => 'checkbox',
    '#title' => t( 'Enable Node' ),
    '#description' => t( 'create node http://exampe.com/drumbleviewer' ),
    '#default_value' => variable_get( 'drumbleviewer_shownode', 1 ),
  );
  
  // show footer
  
  // show title
  
  // show panel
  
  // height
  
  // use fxheight
  
  return system_settings_form ( $form );
}