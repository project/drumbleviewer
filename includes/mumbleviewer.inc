<?php


/*
 * 
 * Mumbleviewer functions convertet to drupal functions by ozon < ozon@mumbleviewer.org >
 * @author PeteSahad , ozon < ozon@mumbleviewer.org > , Urmel < urmel@mumblewiever.org >
 * 
 */

/*
 * return a complede rendered viewer
 */
function drumbleviewer_get_viewer($flags) {
  
  // get default server config
  $default = murmur_getDefaultConf();
  
  // connecr to a server with ID
  $server = murmur_getServer($flags ['serverid'] );
  
  //render viewer
  $output = drumbleviewer_render_viewer($server, $default, $flags );
  
  return $output;
}

$level [- 1] = 0;

function drumbleviewer_render_viewer($server, $default, $flags) {
  global $players;
  global $name;
  global $channels;
  global $url;
  global $tmp_url;
  
  $url = $flags['serverurl'];
  
  $tmp_url = $url;
  // get/set url and port
  $port = $server->getConf('port' );
  if (! $port) {
    $port = $default ['port'] + $server->id() - 1;
    ;
  }
  $tmp_url = $url . ':' . $port . '/';
  
  $name = $server->getConf('registername' );
  if (! $name) {
    $name = $default ["registername"];
  }
  $channels = $server->getChannels();
  
  $players = $server->getPlayers();
  
  // with ajax refresh
  if ($flags ['refresh']) {
    $output .= '<div id="mvrefresh">';
  }
  // set drumbleviewer height
  if ($flags ['simpleheight']) {
    $output .= '<div class="mumbleviewer" style="height:' . $flags ['simpleheight'] . 'px; overflow-x:hidden;">' . "\n";
  } else {
    // eyecandy scroller ;)
    if ($flags ['height']) {
      $output .= '<div id="mv"> <div id="up">up</div>' . "\n";
      $output .= '<div class="mumbleviewer" style="height:' . $flags ['height'] . 'px; overflow-y:hidden; overflow-x:hidden;">' . "\n";
    } else {
      $output .= '<div class="mumbleviewer">' . "\n";
    }
  
  }
  
  $output .= '<div class="div_server"><span class="text_server">' . $name . '</span></div>' . "\n";
  
  // build panel
  if ($flags ['panel']) {
    $output .= '<div class="mv_panel">' . render_panel($server->id(), $server->getConf('password' ) ) . '</div>';
  }
  
  $output .= drumbleviewer_print_users(0, drumbleviewer_has_children(0 ) );
  $output .= drumbleviewer_print_tree($channels, 0 );
  
  $output .= '<div class="div_serverspace"></div>' . "\n";
  
  $output .= '<div id="mv_footer"><div class="text_footer"><a title="MumbleViewer" class="load-mv_footbox" href="http://mumbleviewer.org" rel="#mv_footbox">MUMBLEVIEWER v1.1</a></div>' . drumbleviewer_get_footbox() . '</div></div>' . "\n";
  
  //$output .= '</div>' . "\n";
  

  if ($flags ['height']) {
    $output .= '<div id="down">down</div></div>' . "\n";
  }
  
  if ($flags ['refresh']) {
    $output .= '</div>' . "\n";
  }
  
  return $output;
}

//Mainfunction: print tree structure
function drumbleviewer_print_tree($channels, $parentid) {
  
  global $level;
  global $printline;
  global $fullname;
  
  $children = drumbleviewer_children($channels, $parentid );
  
  if (! empty($children )) {
    foreach ($children as $child => $channel) {
      
      $lastchannel = end($children );
      
      $fullname [$child] = $fullname [$parentid] . '' . $channel [name] . '/';
      
      $has_children = drumbleviewer_children($channels, $child );
      
      if (! empty($has_children )) {
        $has_children = 1;
      } else {
        $has_children = 0;
      }
      
      if ($channel [id] == $lastchannel [id]) {
        $last_channel = 1;
      }
      $output .= '<div class="div_clear"></div>';
      
      if ($channel [id] == $lastchannel [id]) {
        $printline [$level [$child]] = 0;
      } 

      else {
        $printline [$level [$child]] = 1;
      }
      
      for($i = 1; $i <= $level [$child] - 1; $i ++) {
        
        if ($printline [$i] == 1) {
          $output .= '<div class="div_space tree_line image"></div>';
        } 

        else {
          $output .= '<div class="div_space list_empty image"></div>';
        }
      }
      
      //last branch infront of channel
      if ($last_channel) {
        if ($has_children || drumbleviewer_has_users($child ))
          $output .= '<div id="cb-' . $child . '" class="pfeil div_space tree_plus_end image"></div>';
        else
          $output .= '<div class="div_space tree_end image"></div>';
      } else {
        if ($has_children || drumbleviewer_has_users($child ))
          $output .= '<div id="cb-' . $child . '" class="pfeil div_space tree_plus image"></div>';
        else
          $output .= '<div class="div_space tree_mid image"></div>';
      }
      
      //channel symbol and channel name
      $output .= '<div class="div_channel">' . drumbleviewer_channelurl($channel [name], '', $fullname [$child] ) . '</div>';
      
      if ($has_children || drumbleviewer_has_users($child )) {
        
        if (drumbleviewer_has_users($child )) {
          $userinoutput = 'user_in';
          $output .= '<div id="c-cb-' . $child . '" class="childbox ' . $userinoutput . '">' . "\n";
        } else {
          $output .= '<div id="c-cb-' . $child . '" class="childbox empty">' . "\n";
        }
      }
      
      $output .= drumbleviewer_print_users($child, drumbleviewer_has_children($child ) );
      $output .= drumbleviewer_print_tree($channels, $child );
      
      if ($has_children || drumbleviewer_has_users($child )) {
        $output .= '</div>';
      }
    
    }
  }
  return $output;
}

//Return all children of an ID
function drumbleviewer_children($channels, $id) {
  global $level;
  
  foreach ($channels as $c) {
    
    //if pid = id, push into array
    if ($c->parent == $id) {
      
      $children [$c->id] [id] = $c->id;
      $children [$c->id] [name] = $c->name;
      
      $sortArray = array();
      foreach ($children as $key => $array) {
        $sortArray [$key] = $array [name];
      }
      
      array_multisort($sortArray, SORT_ASC, SORT_REGULAR, $children );
      
      $newArray = array();
      foreach ($children as $key => $array) {
        $newArray [$array [id]] [id] = $array [id];
        $newArray [$array [id]] [name] = $array [name];
      }
      
      $children = $newArray;
      
      //set level of child
      $level [$c->id] = $level [$id] + 1;
    
    }
  }
  
  return $children;
}

//display all users of a given channel
function drumbleviewer_print_users($channelid, $has_children) {
  global $players;
  global $level;
  global $printline;
  
  //Look for players in channel and sort them
  foreach ($players as $player) {
    
    if ($player->channel == $channelid) {
      $users [$player->session] = $player->name;
    }
    
    if (! empty($users )) {
      asort($users );
    
    }
  }
  
  //display users
  if (! empty($users )) {
    $output .= '<div class="userblock ' . $channelid . '">';
    
    foreach ($users as $session => $username) {
      $output .= '<div class="div_clear"></div>';
      
      //Spaces
      for($i = 1; $i <= $level [$channelid]; $i ++) {
        
        if ($printline [$i] == 1) {
          $output .= '<div class="div_space tree_line image"></div>';
        } 

        else {
          $output .= '<div class="div_space list_empty image"></div>';
        }
      }
      //last branch infront of an user
      if ($username == end($users ) && $has_children == 0) {
        $output .= '<div class="div_space tree_end image"></div>';
      } else {
        $output .= '<div class="div_space tree_mid image"></div>';
      }
      
      //html chars for users;
      $username = drumbleviewer_html_chars($username );
      
      $output .= '<div class="div_user" id="t-' . $session . '">';
      $output .= drumbleviewer_render_userstats($username, $session );
      $output .= '<a title="' . $username . '" class="load-ustats user_name" href="#ustats=' . $session . '" rel="#ustats-' . $session . '">' . $username . '</a>';
      $output .= drumbleviewer_print_userflags($session );
      $output .= '<div class="div_clear"></div>';
    }
    $output .= '</div>';
  }
  return $output;
}

//display all userflags
function drumbleviewer_print_userflags($session) {
  global $players;
  
  if ($players [$session]->playerid != - 1) {
    $output .= '<div class="userstatus_flags userstatus_flags_authenticated"></div>';
  }
  
  if ($players [$session]->suppressed == 1) {
    $output .= '<div class="userstatus_flags userstatus_flags_muted_server"></div>';
  }
  
  if ($players [$session]->mute == 1) {
    $output .= '<div class="userstatus_flags userstatus_flags_muted_server"></div>';
  }
  
  if ($players [$session]->deaf == 1) {
    $output .= '<div class="userstatus_flags userstatus_flags_deafened_server"></div>';
  }
  
  if ($players [$session]->selfMute == 1) {
    $output .= '<div class="userstatus_flags userstatus_flags_muted_self"></div>';
  }
  
  if ($players [$session]->selfDeaf == 1) {
    $output .= '<div class="userstatus_flags userstatus_deafened_self"></div>';
  }
  
  $output .= '</div>';
  return $output;
}

//boolean, if channel has children 
function drumbleviewer_has_children($id) {
  global $channels;
  $has_children = drumbleviewer_children($channels, $id );
  
  if (! empty($has_children )) {
    return true;
  }
  
  return false;
}

//boolean, if channel has users in it
function drumbleviewer_has_users($channelid) {
  global $players;
  
  foreach ($players as $player) {
    if ($player->channel == $channelid) {
      return true;
    }
  }
  
  return false;
}

//render channel name with URL
function drumbleviewer_channelurl($channelname, $u, $fullchannelname) {
  global $tmp_url;
  
  $output = '<a class="title" href="mumble://' . $tmp_url . rawurlencode($fullchannelname ) . '" title="Join: ' . drumbleviewer_html_chars($channelname ) . '">' . $u . drumbleviewer_html_chars($channelname ) . '</a>';
  return $output;
}

//correct html chars
function drumbleviewer_html_chars($string) {
  $string = htmlentities($string, ENT_QUOTES, 'UTF-8' );
  
  return $string;
}

//onlinetime of an user
function drumbleviewer_get_timeonline($session) {
  global $players;
  $onlinesecs = $players [$session]->onlinesecs;
  
  $onlinesecs = $onlinesecs % (60 * 60 * 24);
  $hours = intval($onlinesecs / (60 * 60) );
  $onlinesecs = $onlinesecs % (60 * 60);
  $mins = intval($onlinesecs / 60 );
  $onlinesecs = $onlinesecs % 60;
  
  if (strlen($hours ) == 1) {
    $hours = '0' . $hours;
  }
  if (strlen($mins ) == 1) {
    $mins = '0' . $mins;
  }
  if (strlen($onlinesecs ) == 1) {
    $onlinesecs = '0' . $onlinesecs;
  }
  $time = $hours . ':' . $mins . ':' . $onlinesecs;
  $output .= $time;
  return $output;

}

// return rendered user stats
function drumbleviewer_render_userstats($username, $session) {
  $output .= '<div id="ustats-' . $session . '">';
  //$output .= $username;
  $output .= '<div class="stats_online"><b>Time online:</b> ' . drumbleviewer_get_timeonline($session ) . '</div>';
  $output .= '<div class="stats_visitor"><b>Visitor:</b> #' . $session . '</div>';
  $output .= '</div>';
  
  return $output;
}

function drumbleviewer_get_footbox() {
  $output = '<div id="mv_footbox">';
  $output .= variable_get('drumbleviewer_footermessage', 'powered by MumbleViewer' );
  $output .= '</div>';
  
  return $output;
}