
Installation
------------

Copy the folder drumbleviewer to your module directory and then enable it in the admin
modules page.

Author
------
Harry Gabriel
support@mumble-tower.de
